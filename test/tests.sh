#!/bin/bash
echo "This is the test harness for the calculator program"
echo "The user input and expected outcomes are specified for each test case"
echo "Since there are 2 possible functionalities of our program based on user input, the test cases must reflect this and hence there are 2 versions of each test category so that we can test both addition and multiplication functionalities"
echo ""

echo "----------------------------------------"
echo "Test case 1a: input <= 9, adding"
INPUT="9 1 2 3 4 5 6 7 8 9 1"
echo "Input:" $INPUT
echo $INPUT | ../src/./calculator 1
echo "Expected outcome: 45"
echo "----------------------------------------"
echo ""


echo "Test case 1b: input <= 9, multiplying"
echo "Input:" $INPUT
echo $INPUT | ../src/./calculator 2
echo "Expected outcome: 362 880"
echo "----------------------------------------"
echo ""

echo "Test case 2a: input == 10, adding"
INPUT="10 1 2 3 4 5 6 7 8 9 10 11"
echo "Input:" $INPUT
echo $INPUT | ../src/./calculator 1
echo "Expected outcome: 55"
echo "----------------------------------------"
echo ""


echo "Test case 2b: input == 10, multiplying"
echo "Input:" $INPUT
echo $INPUT | ../src/./calculator 2
echo "Expected outcome: 3628800"
echo "----------------------------------------"
echo ""

echo "Test case 3a: input > 10, adding"
INPUT="11 9 1 2 3 4 5 6 7 8 9 10"
echo "Input:" $INPUT
echo $INPUT | ../src/./calculator 1
echo "Expected outcome: 45"
echo "----------------------------------------"
echo ""


echo "Test case 3b: input > 10, multiplying"
echo "Input:" $INPUT
echo $INPUT | ../src/./calculator 2
echo "Expected outcome: 362880" 
echo "----------------------------------------"
echo ""


echo "Test case 4: input <= 9, invalid command line argument"
INPUT="9 0 1 2 3 4 5 6 7 8 9"
echo "Input:" $INPUT
echo  $INPUT | ../src/./calculator 0
echo "Expected outcome: 'Usage: ./calculator <function>"
echo "                   Substitute 'function' with a 1 for adding or a 2 for multiplying'"
echo "----------------------------------------"
echo ""

