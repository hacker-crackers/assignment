#include <stdio.h>
#include <stdlib.h>

typedef void (*functionptr)(long*, long); 

/********
* Gets shells 
****/
void getShell(char* buf) 
{
	execve("/bin/sh", NULL, NULL);
}

/********
* Sum of the buf based on the size 
****/
void add(long* buf, long size) 
{
    long sum = 0;
    for (long ii = 0; ii < size; ii++) {
        sum += buf[ii];
    }
    printf("%ld\n", sum);
}

/********
* Multiplication of the buf based on the size 
****/
void multiply(long* buf, long size) 
{
    long mul = 1;
    for (long ii = 0; ii < size; ii++) {
        mul *= buf[ii];
    }
    printf("%ld\n", mul);
}

/********
* MAIN
****/
int main(int argc, char* argv[])
{
    
    functionptr funcPtr = NULL; // Declaring the function pointer before buffer 
    printf("Address of funcPtr: %p\n", &funcPtr);  // Facilitate exploting/examine when using gdb
    long buf[10];
    long size; 

    // Command line argument validation
    if (argc != 2) 
    {
        printf("%s","Usage: ./calculator <function>\nSubstitute 'function' with a 1 for adding or a 2 for multiplying\n");
        exit(1);
    } 

    // Assigning funcPtr with selected option
    if (atoi(argv[1]) == 1) 
    {
        funcPtr = &add;
    }
    else if (atoi(argv[1]) == 2) 
    {
        funcPtr = &multiply;
    }
    else 
    {
        printf("%s","Usage: ./calculator <function>\nSubstitute 'function' with a 1 for adding or a 2 for multiplying\n");
        exit(1);
    }

    // Prompt users for the size of numbers they want to add/multiply
    printf("Please enter a range of numbers to add (max 10 numbers): ");
    scanf("%ld", &size); 
    printf("\n");

    // Validate that the size is between 1 & 10
    while (size > 10 || size < 1) 
    {
        printf("Please ensure that it is between 1 & 10 number/s\n");
        scanf("%ld", &size); 
    }

    // Add the desired numbers to add/multiply into the buffer
    for (int ii = 0; ii <= size; ii++) // note the <= sign is the off-by-one error 
    {
        printf("num[%d]:", ii);
        scanf("%ld", &buf[ii]);
        printf("\n");
    }
    
    // Dereference and call the function pointer
    (*funcPtr)(buf, size);
    return 0; 
}