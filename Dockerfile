FROM ubuntu:18.04

# Install app dependencies
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y gcc make 

COPY src /root/program/src
COPY test /root/program/test

WORKDIR /root/program
COPY src /app
RUN make -C /root/program/src